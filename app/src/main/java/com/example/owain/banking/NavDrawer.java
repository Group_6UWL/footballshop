package com.example.owain.banking;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class NavDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static String username;
    public static double balance;
    public static int pin;
    public static String firstName;
    public static String lastName;
    public static String message;
    public static String permissions;
    public static int uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //instantiates intent
        Intent intent = getIntent();

        //gets the data passed from the main activity
        username = intent.getStringExtra("username");
        pin = intent.getIntExtra("pin", 0);
        firstName = intent.getStringExtra("first_name");
        lastName = intent.getStringExtra("last_name");
        balance = intent.getDoubleExtra("balance", 0.00);
        permissions = intent.getStringExtra("permissions");
        uid = intent.getIntExtra("userId", 0);


        super.onCreate(savedInstanceState);
        int resID = R.layout.activity_nav_drawer;;

        if(permissions.toUpperCase().contains("ADMIN")) {
            resID = R.layout.activity_admin_nav_drawer;
        }

        setContentView(resID);
        //finds elements and attaches them to variable

        //gets toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //instantiates nav drawer layout
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fragmentManager = getFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putInt("id", uid);
        HomeFrag fragobj = new HomeFrag();
        fragobj.setArguments(bundle);
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragobj).commit();

        //response listener for the to get the request response
        Response.Listener<String> responseListener = new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                //Turn response into json and check it is successful
                try {
                    JSONObject json = new JSONObject(response);
                    boolean success = json.getBoolean("success");

                    if(!success){
                        //If Unsuccessfull show alert box saying login unsuccessful
                        AlertDialog.Builder builder = new AlertDialog.Builder(NavDrawer.this);
                        builder.setMessage("Login Was Not Successful")
                                .setNegativeButton("Retry", null)
                                .create()
                                .show();
                    }
                }
                catch(JSONException e){
                    //Catch JSON error
                    e.printStackTrace();
                }
            }
        };

        //Set Up Cart
        setUpCartReq cartReq = new setUpCartReq(uid, responseListener);
        RequestQueue queue = Volley.newRequestQueue(NavDrawer.this);
        queue.add(cartReq);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    //Function To Determine which option has been selected
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fragmentManager = getFragmentManager();
        //Conditional to see which selection
        if (id == R.id.nav_shirts_layout) {
            //opens pay fragment
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ShirtsFrag()).commit();
        }
        else if(id == R.id.nav_home_layout){
            //opens home fragment
            Bundle bundle = new Bundle();
            bundle.putInt("id", uid);
            HomeFrag fragobj = new HomeFrag();
            fragobj.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragobj).commit();
        }else if (id == R.id.nav_shorts_layout) {
            //opens user fragment
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ShortsFrag()).commit();
        }else if (id == R.id.nav_hats_layout) {
            //opens user fragment
            fragmentManager.beginTransaction().replace(R.id.content_frame, new HatsFrag()).commit();
        }else if (id == R.id.nav_bags_layout) {
            //opens user fragment
            fragmentManager.beginTransaction().replace(R.id.content_frame, new BagsFrag()).commit();
        }
        else if (id == R.id.nav_admin_layout){
            fragmentManager.beginTransaction().replace(R.id.content_frame, new AdminFrag()).commit();
        }
        //open trolley fragment
        else if (id == R.id.nav_trolley_layout){
            fragmentManager.beginTransaction().replace(R.id.content_frame, new CheckoutFrag()).commit();
        }
        //closes the drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
