package com.example.owain.banking;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by owain on 13/12/2016.
 */

public class HomeFrag extends Fragment{

    public void openShirt(String team){
        Bundle bundle = new Bundle();
        bundle.putString("team", team);
        Fragment shirtFrag = new ShirtItemFrag();
        shirtFrag.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, shirtFrag).commit();
    }

    public void openShorts(String team){
        Bundle bundle = new Bundle();
        bundle.putString("team", team);
        Fragment shortsFrag = new ShortsItemFrag();
        shortsFrag.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, shortsFrag).commit();
    }

    public void openBag(String team){
        Bundle bundle = new Bundle();
        bundle.putString("team", team);
        Fragment bagFrag = new BagItemFrag();
        bagFrag.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, bagFrag).commit();
    }

    public void openHat(String team){
        Bundle bundle = new Bundle();
        bundle.putString("team", team);
        Fragment hatFrag = new HatItemFrag();
        hatFrag.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, hatFrag).commit();
    }

    //func to show error dialog box
    private void showErr(String err){
        //If Unsuccessfull show alert box saying login unsuccessful
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(err)
                .setNegativeButton("Close", null)
                .create()
                .show();
    }
    //sets global variable
    public static String message;
    public static double newBal = NavDrawer.balance;

    //Instantiates View
    View myView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int uid = getArguments().getInt("id");
        //sets View
        myView = inflater.inflate(R.layout.home, container, false);

        //finds elements from layout
        final TextView fName = (TextView) myView.findViewById(R.id.tvFName);
        final TextView lName = (TextView) myView.findViewById(R.id.tvLName);
        final TextView email = (TextView) myView.findViewById(R.id.tvEmail);
        final TextView balance = (TextView) myView.findViewById(R.id.tvBalance);
        final TextView points = (TextView) myView.findViewById(R.id.tvPoints);
        final TextView purch = (TextView) myView.findViewById(R.id.tvPurch);

        final ImageButton arsenalShorts = (ImageButton) myView.findViewById(R.id.arsShorts);
        final ImageButton chsShirt = (ImageButton) myView.findViewById(R.id.chsShirt);
        final ImageButton lplShirt = (ImageButton) myView.findViewById(R.id.lplShirt);
        final ImageButton mciBag = (ImageButton) myView.findViewById(R.id.mciBag);
        final ImageButton totHat = (ImageButton) myView.findViewById(R.id.totHat);
        final ImageButton chsHat = (ImageButton) myView.findViewById(R.id.chsHat);
        final ImageButton totShirt = (ImageButton) myView.findViewById(R.id.totShirt);

        arsenalShorts.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShorts("ars");
            }
        });

        chsShirt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShorts("chs");
            }
        });

        lplShirt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShorts("lvp");
            }
        });

        mciBag.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openBag("mci");
            }
        });

        totHat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openHat("th");
            }
        });

        chsHat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openHat("chs");
            }
        });

        totShirt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShirt("th");
            }
        });




        Response.Listener<String> responseListener = new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                //Turn response into json and check it is successful
                try {

                    JSONObject json = new JSONObject(response);
                    boolean success = json.getBoolean("success");

                    if(success){
                        //Assign JSON fields to variables
                        fName.setText(json.getString("first_name"));
                        lName.setText(json.getString("last_name"));
                        email.setText(json.getString("email"));
                        balance.setText(json.getDouble("balance")+"");
                        points.setText(json.getInt("points")+"");
                        if(json.getInt("points") == 0){
                            purch.setText("10");
                        }
                        else{
                            int purchCal = 100 - json.getInt("points");
                            if(purchCal == 0){
                                purch.setText("You Have A Bonus Waiting!");
                            }
                            else{
                                purchCal = purchCal/10;
                                purchCal = 10 - purchCal;
                                purch.setText(purchCal+"");
                            }
                        }
                    }
                    else{
                        //If Unsuccessfull show alert box saying login unsuccessful
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Could Not Get User Data")
                                .setNegativeButton("Close", null)
                                .create()
                                .show();
                    }
                }
                catch(JSONException e){
                    //Catch JSON error
                    e.printStackTrace();
                }
            }
        };

        //sends request to get all shorts stock levels
        getUserInfoReq user = new getUserInfoReq(uid, responseListener);
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.add(user);
        return myView;
    }
}
