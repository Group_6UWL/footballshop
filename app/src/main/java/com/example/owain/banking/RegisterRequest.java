package com.example.owain.banking;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by owain on 12/12/2016.
 */

public class RegisterRequest extends StringRequest{
    //Sets Request URL
    private static final String REGISTER_REQUEST_URL = "https://groupprojectuwl.000webhostapp.com/Register.php";
    //Instantiates Map
    private Map<String, String> params;
    public RegisterRequest(String firstName, String lastName, String email, String password, String address, long phone, double sBal, Response.Listener<String> listener){
        super(Request.Method.POST, REGISTER_REQUEST_URL, listener, null);
        //Sets Params
        params = new HashMap<>();
        params.put("first_name", firstName);
        params.put("last_name", lastName);
        params.put("email", email);
        params.put("password", password);
        params.put("address", address);
        params.put("phone_number", phone+"");
        params.put("balance", sBal+"");
    }

    @Override
    //Gets Params
    public Map<String, String> getParams() {
        return params;
    }
}
