package com.example.owain.banking;

/**
 * Created by owain on 14/02/2017.
 */

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class HatsFrag extends Fragment{
    public void openHat(String team){
        Bundle bundle = new Bundle();
        bundle.putString("team", team);
        Fragment hatFrag = new HatItemFrag();
        hatFrag.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, hatFrag).commit();
    }

    //Instantiates View
    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Sets View
        myView = inflater.inflate(R.layout.hats, container, false);

        final ImageButton arsenalHat = (ImageButton) myView.findViewById(R.id.arsHat);
        final ImageButton chelseaHat = (ImageButton) myView.findViewById(R.id.chsHat);
        final ImageButton liverpoolHat = (ImageButton) myView.findViewById(R.id.lvpHat);
        final ImageButton mcityHat = (ImageButton) myView.findViewById(R.id.mciHat);
        final ImageButton totHat = (ImageButton) myView.findViewById(R.id.ttHat);

        arsenalHat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openHat("ars");
            }
        });

        chelseaHat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openHat("chs");
            }
        });

        liverpoolHat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openHat("lvp");
            }
        });

        mcityHat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openHat("mci");
            }
        });

        totHat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openHat("th");
            }
        });



        return myView;
    }
}
