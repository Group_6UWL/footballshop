package com.example.owain.banking;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by owain on 14/03/2017.
 */

public class ShortsItemFrag extends Fragment{
    View myView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.shortsitem, container, false);

        TextView shortsName = (TextView) myView.findViewById(R.id.txtName);
        TextView shortsPrice = (TextView) myView.findViewById(R.id.txtPrice);
        TextView shortsDesc = (TextView) myView.findViewById(R.id.txtDescription);
        ImageView image = (ImageView) myView.findViewById(R.id.imgBag);

        String team = getArguments().getString("team");

        if(team == "ars"){
            shortsName.setText("Arsenal Shorts");
            shortsPrice.setText("£19.99");
            shortsDesc.setText("Arsenal Shorts Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.ars_shorts);
        }
        else if(team == "chs"){
            shortsName.setText("Chelsea Shorts");
            shortsPrice.setText("£17.99");
            shortsDesc.setText("Chelsea Shorts Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.chs_shorts);
        }
        else if(team == "lvp"){
            shortsName.setText("Liverpool Shorts");
            shortsPrice.setText("£19.99");
            shortsDesc.setText("Liverpool Shorts Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.lvp_shorts);
        }
        else if(team == "mci"){
            shortsName.setText("Manchester Shorts");
            shortsPrice.setText("£22.99");
            shortsDesc.setText("Manchester City Shorts Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.mci_shorts);
        }
        else if(team == "th"){
            shortsName.setText("Tottenham Shorts");
            shortsPrice.setText("£21.00");
            shortsDesc.setText("Tottenham Shorts Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.tt_shorts);
        }
        else{
            shortsName.setText("Error");
            shortsPrice.setText("Error");
            shortsDesc.setText("Error");
        }
        return myView;
    }
}
