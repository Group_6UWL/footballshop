package com.example.owain.banking;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;
/**
 * Created by owain on 09/04/2017.
 */

public class getUserInfoReq extends StringRequest{
    //Sets Request URL
    private static final String USER_INFO_REQUEST_URL = "https://groupprojectuwl.000webhostapp.com/getUserInfo.php";
    //Instantiates Map
    private Map<String, String> params;

    public getUserInfoReq(int id,Response.Listener<String> listener){
        super(Request.Method.POST, USER_INFO_REQUEST_URL, listener, null);
        //Inserts Params
        params = new HashMap<>();
        params.put("id", String.valueOf(id));
    }

    @Override
    //returns params
    public Map<String, String> getParams() {
        return params;
    }
}
