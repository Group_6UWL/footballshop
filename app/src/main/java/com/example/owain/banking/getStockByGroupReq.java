package com.example.owain.banking;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;
/**
 * Created by owain on 05/04/2017.
 */



public class getStockByGroupReq extends StringRequest{
    //Sets Request URL
    private static final String STOCK_GROUP_REQUEST_URL = "https://groupprojectuwl.000webhostapp.com/getGroupStock.php";
    //Instantiates Map
    private Map<String, String> params;

    public getStockByGroupReq(String sGroup,Response.Listener<String> listener){
        super(Request.Method.POST, STOCK_GROUP_REQUEST_URL, listener, null);
        //Inserts Params
        params = new HashMap<>();
        params.put("stockGroup", sGroup);
    }

    @Override
    //returns params
    public Map<String, String> getParams() {
        return params;
    }
}
