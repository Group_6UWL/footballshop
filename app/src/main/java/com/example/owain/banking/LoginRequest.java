package com.example.owain.banking;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;


public class LoginRequest extends StringRequest {
    //Sets Request URL
    private static final String LOGIN_REQUEST_URL = "https://groupprojectuwl.000webhostapp.com/Login.php";
    //Instantiates Map
    private Map<String, String> params;

    public LoginRequest(String username,String pin, Response.Listener<String> listener){
        super(Request.Method.POST, LOGIN_REQUEST_URL, listener, null);
        //Inserts Params
        params = new HashMap<>();
        params.put("username", username);
        params.put("password", pin );
    }

    @Override
    //returns params
    public Map<String, String> getParams() {
        return params;
    }
}

