package com.example.owain.banking;

/**
 * Created by owain on 14/02/2017.
 */

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
public class ShortsFrag extends Fragment{
    public void openShorts(String team){
        Bundle bundle = new Bundle();
        bundle.putString("team", team);
        Fragment shortsFrag = new ShortsItemFrag();
        shortsFrag.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, shortsFrag).commit();
    }

    //Instantiates View
    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Sets View
        myView = inflater.inflate(R.layout.shorts, container, false);

        final ImageButton arsenalShorts = (ImageButton) myView.findViewById(R.id.arsShorts);
        final ImageButton chelseaShorts = (ImageButton) myView.findViewById(R.id.chsShorts);
        final ImageButton liverpoolShorts = (ImageButton) myView.findViewById(R.id.lvpShorts);
        final ImageButton mcityShorts = (ImageButton) myView.findViewById(R.id.mciShorts);
        final ImageButton totShorts = (ImageButton) myView.findViewById(R.id.ttShorts);

        arsenalShorts.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShorts("ars");
            }
        });

        chelseaShorts.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShorts("chs");
            }
        });

        liverpoolShorts.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShorts("lvp");
            }
        });

        mcityShorts.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShorts("mci");
            }
        });

        totShorts.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShorts("th");
            }
        });



        return myView;
    }
}
