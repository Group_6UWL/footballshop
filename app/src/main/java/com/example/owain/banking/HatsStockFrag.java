package com.example.owain.banking;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by owain on 21/03/2017.
 */

public class HatsStockFrag extends Fragment{
    public void updateStock(int stockID, int updateAm){

        Response.Listener<String> updateListener = new Response.Listener<String>(){
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    boolean success = json.getBoolean("success");

                    if(success == true){
                        showErr("Stock Added Successfully!");
                    }
                    else{
                        showErr("Stock Could Not Be Added! Please Try Again.");
                    }
                }
                catch (JSONException e) {
                    //Catch JSON error
                    e.printStackTrace();
                }
            }
        };

        //sends request to get all shirts stock levels
        updateStockReq updateReq = new updateStockReq(stockID, updateAm, updateListener);
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.add(updateReq);
    }

    private void getSingleItemStock(final int id, final int tv){
        //finding elements by id
        final TextView asStockTxt = (TextView) myView.findViewById(R.id.txtAsStockLvl);
        final TextView chStockTxt = (TextView) myView.findViewById(R.id.txtChStockLvl);
        final TextView lvStockTxt= (TextView) myView.findViewById(R.id.txtLvStockLvl);
        final TextView mcStockTxt = (TextView) myView.findViewById(R.id.txtMcStockLvl);
        final TextView ttStockTxt = (TextView) myView.findViewById(R.id.txtTtStockLvl);

        final TextView[] tvArr = new TextView[5];
        tvArr[0] = asStockTxt;
        tvArr[1] = chStockTxt;
        tvArr[2] = lvStockTxt;
        tvArr[3] = mcStockTxt;
        tvArr[4] = ttStockTxt;

        final Response.Listener<String> responseListener = new Response.Listener<String>(){
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    boolean success = json.getBoolean("success");

                    if(success){
                        int sLvl = json.getInt("stock_level");
                        tvArr[tv].setText(sLvl+"");
                    }
                    else{
                        showErr("Could Not Get Stock Levels");
                        tvArr[tv].setText("Error");
                    }
                } catch (JSONException e) {
                    //Catch JSON error
                    e.printStackTrace();
                }
            }
        };

        //pause for two seconds to ensure table has been updated
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                //sends request to get all shirts stock levels
                getSingleItemStockLevelReq stockReq = new getSingleItemStockLevelReq(id, responseListener);
                RequestQueue queue = Volley.newRequestQueue(getActivity());
                queue.add(stockReq);
            }
        }, 2000);

    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }

    //func to show error dialog box
    private void showErr(String err){
        //If Unsuccessfull show alert box saying login unsuccessful
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(err)
                .setNegativeButton("Close", null)
                .create()
                .show();
    }

    //Instantiates View
    View myView;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Sets View
        myView = inflater.inflate(R.layout.hats_stock, container, false);

        final TextView asStockTxt = (TextView) myView.findViewById(R.id.txtAsStockLvl);
        final TextView chStockTxt = (TextView) myView.findViewById(R.id.txtChStockLvl);
        final TextView lvStockTxt = (TextView) myView.findViewById(R.id.txtLvStockLvl);
        final TextView mcStockTxt = (TextView) myView.findViewById(R.id.txtMcStockLvl);
        final TextView ttStockTxt = (TextView) myView.findViewById(R.id.txtToStockLvl);

        final Button btnArs = (Button) myView.findViewById(R.id.btnArsUpdate);
        final Button btnChs = (Button) myView.findViewById(R.id.btnChsUpdate);
        final Button btnLpl = (Button) myView.findViewById(R.id.btnLvpUpdate);
        final Button btnMci = (Button) myView.findViewById(R.id.btnMcUpdate);
        final Button btnThs = (Button) myView.findViewById(R.id.btnTotUpdate);

        final EditText etArs = (EditText) myView.findViewById(R.id.etArsStock);
        final EditText etChs = (EditText) myView.findViewById(R.id.etChsStock);
        final EditText etLpl = (EditText) myView.findViewById(R.id.etLvStock);
        final EditText etMci = (EditText) myView.findViewById(R.id.etMcStock);
        final EditText etThs = (EditText) myView.findViewById(R.id.etToStock);

        btnArs.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Hides Keyboard
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                if(!isEmpty(etArs)){
                    asStockTxt.setText("Loading...");
                    final int arsS = Integer.parseInt(etArs.getText().toString());
                    etArs.setText("");
                    updateStock(18, arsS);
                    getSingleItemStock(18,0);
                }
                else{
                    showErr("Please Enter A Value");
                }
            }
        });

        btnChs.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Hides Keyboard
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                if(!isEmpty(etChs)) {
                    chStockTxt.setText("Loading...");
                    final int chsS = Integer.parseInt(etChs.getText().toString());
                    etChs.setText("");
                    updateStock(19, chsS);
                    getSingleItemStock(19,1);
                }
                else{
                    showErr("Please Enter A Value");
                }
            }
        });

        btnLpl.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Hides Keyboard
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                if(!isEmpty(etLpl)) {
                    lvStockTxt.setText("Loading...");
                    final int lplS = Integer.parseInt(etLpl.getText().toString());
                    etLpl.setText("");
                    updateStock(20, lplS);
                    getSingleItemStock(20,2);
                }
                else{
                    showErr("Please Enter A Value");
                }
            }
        });

        btnMci.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Hides Keyboard
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                if(!isEmpty(etMci)) {
                    mcStockTxt.setText("Loading...");
                    final int mciS = Integer.parseInt(etMci.getText().toString());
                    etMci.setText("");
                    updateStock(21, mciS);
                    getSingleItemStock(21,3);
                }
                else{
                    showErr("Please Enter A Value");
                }
            }
        });

        btnThs.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Hides Keyboard
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                if(!isEmpty(etThs)){
                    ttStockTxt.setText("Loading..");
                    final int thsS = Integer.parseInt(etThs.getText().toString());
                    etThs.setText("");
                    updateStock(22,thsS);
                    getSingleItemStock(22,4);
                }
                else{
                    showErr("Please Enter A Value");
                }
            }
        });

        Response.Listener<String> responseListener = new Response.Listener<String>(){
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    boolean success = json.getBoolean("success");

                    if(success){
                        JSONObject arsObj = json.getJSONObject("ars");
                        JSONObject chsObj = json.getJSONObject("chs");
                        JSONObject lplObj = json.getJSONObject("lpl");
                        JSONObject mciObj = json.getJSONObject("mci");
                        JSONObject thsObj = json.getJSONObject("ths");

                        StringBuilder arsStocksb = new StringBuilder();
                        StringBuilder chsStocksb = new StringBuilder();
                        StringBuilder lplStocksb = new StringBuilder();
                        StringBuilder mciStocksb = new StringBuilder();
                        StringBuilder thsStocksb = new StringBuilder();

                        arsStocksb.append(arsObj.getInt("stock_level"));
                        chsStocksb.append(chsObj.getInt("stock_level"));
                        lplStocksb.append(lplObj.getInt("stock_level"));
                        mciStocksb.append(mciObj.getInt("stock_level"));
                        thsStocksb.append(thsObj.getInt("stock_level"));

                        asStockTxt.setText(arsStocksb.toString());
                        chStockTxt.setText(chsStocksb.toString());
                        lvStockTxt.setText(lplStocksb.toString());
                        mcStockTxt.setText(mciStocksb.toString());
                        ttStockTxt.setText(thsStocksb.toString());
                    }
                    else{
                        //If Unsuccessfull show alert box saying login unsuccessful
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("Could Not Get Stock Levels")
                                .setNegativeButton("Close", null)
                                .create()
                                .show();
                    }
                } catch (JSONException e) {
                    //Catch JSON error
                    e.printStackTrace();
                }
            }
        };

        //sends request to get all hats stock levels
        getStockByGroupReq stockReq = new getStockByGroupReq("hat", responseListener);
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.add(stockReq);

        return myView;
    }
}
