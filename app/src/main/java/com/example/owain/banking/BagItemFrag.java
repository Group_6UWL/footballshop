package com.example.owain.banking;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.RequestQueue;
import com.android.volley.Response;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.owain.banking.NavDrawer.uid;
import static com.example.owain.banking.R.id.imgBag;

/**
 * Created by owain on 14/03/2017.
 */

public class BagItemFrag extends Fragment{
    View myView;
    public static int uid;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.bagitem, container, false);

        TextView bagName = (TextView) myView.findViewById(R.id.txtName);
        TextView bagPrice = (TextView) myView.findViewById(R.id.txtPrice);
        TextView bagDesc = (TextView) myView.findViewById(R.id.txtDescription);
        ImageView image = (ImageView) myView.findViewById(imgBag);

        final Button btnBuy = (Button) myView.findViewById(R.id.btnBuy);

        String team = getArguments().getString("team");

        if(team == "ars"){
            bagName.setText("Arsenal Bag");
            bagPrice.setText("£22.00");
            bagDesc.setText("Arsenal Bag Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.ars_bag);
        }
        else if(team == "chs"){
            bagName.setText("Chelsea Bag");
            bagPrice.setText("£20.00");
            bagDesc.setText("Chelsea Bag Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.chs_bag);
        }
        else if(team == "lvp"){
            bagName.setText("Liverpool Bag");
            bagPrice.setText("£18.99");
            bagDesc.setText("Liverpool Bag Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.lvp_bag);
        }
        else if(team == "mci"){
            bagName.setText("Manchester City Bag");
            bagPrice.setText("£15.99");
            bagDesc.setText("Manchester City Bag Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.mci_bag);
        }
        else if(team == "th"){
            bagName.setText("Tottenham Bag");
            bagPrice.setText("£20.00");
            bagDesc.setText("Tottenham Bag Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.tt_bag);
        }
        else{
            bagName.setText("Error");
            bagPrice.setText("Error");
            bagDesc.setText("Error");
        }

        btnBuy.setOnClickListener(new View.OnClickListener() {
            //TODO: request to Cart SELECT cartID FROM Cart WHERE userID = ?

            //TODO: INSERT INTO Transaction (itemID, cartID) VALUES( ? , ?)
            TextView titemname = (TextView) myView.findViewById(R.id.txtName);
            TextView tprice = (TextView) myView.findViewById(R.id.txtPrice);
            TextView tdescription= (TextView) myView.findViewById(R.id.txtDescription);

            @Override
            public void onClick(View v) {
                final String item_name = titemname.getText().toString();
                final String price = tprice.getText().toString();
                final String description = tdescription.getText().toString();

                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //Turn response into json and check it is successful
                            JSONObject json = new JSONObject(response);
                            boolean success = json.getBoolean("success");

                            if(success){
                                String name = json.getString("item_name");
                                String desc = json.getString("description");
                                double price =json.getDouble("price");


                                FragmentTransaction transection=getFragmentManager().beginTransaction();
                                CheckoutFrag sfragment = new CheckoutFrag();

                                CheckoutFrag checkout = new CheckoutFrag();
                                Bundle args = new Bundle();
                                args.putString("item name",name);
                                args.putDouble("price",price);
                                args.putString("description", desc);

                                //send data to second fragment
                                checkout.setArguments(args);
                                transection.replace(R.id.imgBag, sfragment);
                                transection.commit();

                            }

                        }
                        catch(JSONException e){
                            e.printStackTrace();
                        }

                    }
                };
                //CartRequest cartReq = new CartRequest(uid, responseListener);
                //RequestQueue queue = Volley.newRequestQueue(BagItemFrag.this);
                //queue.add(cartReq);
            }

        });


        return myView;
    }
}
