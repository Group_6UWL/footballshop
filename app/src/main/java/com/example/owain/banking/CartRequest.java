package com.example.owain.banking;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import static android.R.attr.id;

public class CartRequest extends StringRequest {
    //Sets Request URL
    private static final String CART_REQUEST_URL = "https://groupprojectuwl.000webhostapp.com/Cart.php";
    //Instantiates Map
    private Map<String, String> params;

    public CartRequest(int cart_id,int user_id, Response.Listener<String> listener){
        super(Request.Method.POST, CART_REQUEST_URL, listener, null);
        //Inserts Params
        params = new HashMap<>();
        params.put("cartID", String.valueOf(cart_id));
        params.put("userID", String.valueOf(user_id));
    }



    @Override
    //returns params
    public Map<String, String> getParams() {
        return params;
    }
}
