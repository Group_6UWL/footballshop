package com.example.owain.banking;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;
/**
 * Created by owain on 06/04/2017.
 */

public class updateStockReq extends StringRequest{
    //Sets Request URL
    private static final String STOCK_UPDATE_REQUEST_URL = "https://groupprojectuwl.000webhostapp.com/updateStock.php";
    //Instantiates Map
    private Map<String, String> params;

    public updateStockReq(int id, int amount, Response.Listener<String> listener){
        super(Request.Method.POST, STOCK_UPDATE_REQUEST_URL, listener, null);
        //Inserts Params
        params = new HashMap<>();
        params.put("item_id", String.valueOf(id));
        params.put("amount", String.valueOf(amount));
    }

    @Override
    //returns params
    public Map<String, String> getParams() {
        return params;
    }
}
