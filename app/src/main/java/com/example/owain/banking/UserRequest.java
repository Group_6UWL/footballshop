package com.example.owain.banking;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by owain on 13/12/2016.
 */

public class UserRequest extends StringRequest {
    //Sets Request URL
    private static final String USER_REQUEST_URL = "https://app-db.000webhostapp.com/User.php";
    //Instantiates Map
    private Map<String, String> params;

    public UserRequest(String fName, String lName, String username, int pin, String olUser, double balance, Response.Listener<String> listener){
        super(Request.Method.POST, USER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        //sets params
        params.put("first_name", fName);
        params.put("last_name", lName);
        params.put("username", username);
        params.put("pin", pin+"");
        params.put("olUser", olUser);
        params.put("balance", balance+"");
    }

    @Override
    //returns params
    public Map<String, String> getParams() {
        return params;
    }
}
