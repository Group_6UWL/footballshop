package com.example.owain.banking;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;
/**
 * Created by owain on 09/04/2017.
 */

public class setUpCartReq  extends StringRequest{
    //Sets Request URL
    private static final String SET_CART_REQUEST_URL = "https://groupprojectuwl.000webhostapp.com/setUpCart.php";
    //Instantiates Map
    private Map<String, String> params;

    public setUpCartReq(int id,Response.Listener<String> listener){
        super(Request.Method.POST, SET_CART_REQUEST_URL, listener, null);
        //Inserts Params
        params = new HashMap<>();
        params.put("id", String.valueOf(id));
    }

    @Override
    //returns params
    public Map<String, String> getParams() {
        return params;
    }
}
