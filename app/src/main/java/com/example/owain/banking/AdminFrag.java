package com.example.owain.banking;

/**
 * Created by owain on 16/03/2017.
 */

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
public class AdminFrag extends Fragment {


    public void openCatFrag(String stock) {
        switch (stock) {
            case "shirt":
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, new ShirtStockFrag());
                ft.commit();
                break;
            case "shorts":
                FragmentManager fmSh = getFragmentManager();
                FragmentTransaction ftSh = fmSh.beginTransaction();
                ftSh.replace(R.id.content_frame, new ShortsStockFrag());
                ftSh.commit();
                break;
            case "hat":
                FragmentManager fmH = getFragmentManager();
                FragmentTransaction ftH = fmH.beginTransaction();
                ftH.replace(R.id.content_frame, new HatsStockFrag());
                ftH.commit();
                break;
            case "bag":
                FragmentManager fmB = getFragmentManager();
                FragmentTransaction ftB = fmB.beginTransaction();
                ftB.replace(R.id.content_frame, new BagsStockFrag());
                ftB.commit();
                break;
            default:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Error")
                        .setNegativeButton("Close", null)
                        .create()
                        .show();
        }
    }
    //Instantiates View
    View myView;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Sets View
        myView = inflater.inflate(R.layout.admin, container, false);

        final Button btnShirt = (Button) myView.findViewById(R.id.btnShirt);
        final Button btnShorts = (Button) myView.findViewById(R.id.btnShorts);
        final Button btnHats = (Button) myView.findViewById(R.id.btnHats);
        final Button btnBags = (Button) myView.findViewById(R.id.btnBags);

        btnShirt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openCatFrag("shirt");
            }
        });


        btnShorts.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openCatFrag("shorts");
            }
        });

        btnHats.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openCatFrag("hat");
            }
        });

        btnBags.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openCatFrag("bag");
            }
        });
        return myView;
    }
}



