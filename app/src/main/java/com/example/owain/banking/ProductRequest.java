package com.example.owain.banking;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class ProductRequest extends StringRequest {
    //Sets Request URL
    private static final String PRODUCT_REQUEST_URL = "https://groupprojectuwl.000webhostapp.com/Product.php";
    //Instantiates Map
    private Map<String, String> params;

    public ProductRequest(String item_name,String price, String des, Response.Listener<String> listener){
        super(Request.Method.POST, PRODUCT_REQUEST_URL, listener, null);
        //Inserts Params
        params = new HashMap<>();
        params.put("id", item_name);
    }

    @Override
    //returns params
    public Map<String, String> getParams() {
        return params;
    }
}

