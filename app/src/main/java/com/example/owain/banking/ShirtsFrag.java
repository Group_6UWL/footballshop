package com.example.owain.banking;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by owain on 14/02/2017.
 */

public class ShirtsFrag extends Fragment{

    public void openShirt(String team){
        Bundle bundle = new Bundle();
        bundle.putString("team", team);
        Fragment shirtFrag = new ShirtItemFrag();
        shirtFrag.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, shirtFrag).commit();
    }


    //Instantiates View
    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Sets View
        myView = inflater.inflate(R.layout.shirts, container, false);
        final ImageButton arsenalShirt = (ImageButton) myView.findViewById(R.id.arsShirt);
        final ImageButton chelseaShirt = (ImageButton) myView.findViewById(R.id.chsShirt);
        final ImageButton liverpoolShirt = (ImageButton) myView.findViewById(R.id.lvpShirt);
        final ImageButton mcityShirt = (ImageButton) myView.findViewById(R.id.mciShirt);
        final ImageButton totShirt = (ImageButton) myView.findViewById(R.id.ttShirt);

        arsenalShirt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShirt("ars");
            }
        });

        chelseaShirt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShirt("chs");
            }
        });

        liverpoolShirt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShirt("lvp");
            }
        });

        mcityShirt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShirt("mci");
            }
        });

        totShirt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openShirt("th");
            }
        });



        return myView;
    }
}
