package com.example.owain.banking;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //finding the different elemets and attaching them to a variable
        final EditText etFName = (EditText) findViewById(R.id.etFName);
        final EditText etLName = (EditText) findViewById(R.id.etLName);
        final EditText etEmail = (EditText) findViewById(R.id.etEmail);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);
        final EditText etAddress = (EditText) findViewById(R.id.etAddress);
        final EditText etPhone = (EditText) findViewById(R.id.etPhone);
        final EditText etSBal = (EditText) findViewById(R.id.etSBal);

        final Button bRegister = (Button) findViewById(R.id.bRegister);

        //setting up an onclick listener for the register button
        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getting inputted data
                final String fName = etFName.getText().toString();
                final String lName = etLName.getText().toString();
                final String email = etEmail.getText().toString();
                final String password = etPassword.getText().toString();
                final String address = etAddress.getText().toString();
                final long phoneNumber = Long.parseLong(etPhone.getText().toString());
                final double balance = Double.parseDouble(etSBal.getText().toString());


                //response listener for the to get the request response
                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //Turn response into json and check it is successful
                            JSONObject json = new JSONObject(response);
                            boolean success = json.getBoolean("success");

                            if(success){
                                //if successsful it returns to the main activity so the use can log in
                                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                RegisterActivity.this.startActivity(intent);
                            }
                            else{
                                //if unsuccessful send alert
                                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                builder.setMessage("Registration Failed")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }
                        } catch(JSONException e){
                            //catach json error
                            e.printStackTrace();
                        }
                    }
                };

                //Login Request class is instantiated and request is added to queue
                RegisterRequest registerRequest = new RegisterRequest(fName,lName,email,password,address,phoneNumber,balance, responseListener);
                RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
                queue.add(registerRequest);
            }
        });
    }
}
