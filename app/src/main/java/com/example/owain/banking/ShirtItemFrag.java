package com.example.owain.banking;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by owain on 14/03/2017.
 */

public class ShirtItemFrag extends Fragment{

    View myView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.shirtitem, container, false);
        TextView shirtName = (TextView) myView.findViewById(R.id.txtName);
        TextView shirtPrice = (TextView) myView.findViewById(R.id.txtPrice);
        TextView shirtDesc = (TextView) myView.findViewById(R.id.txtDescription);
        ImageView shirtImg = (ImageView) myView.findViewById(R.id.imgBag);

        String team = getArguments().getString("team");

        if(team == "ars"){
            shirtName.setText("Arsenal Home Shirt");
            shirtPrice.setText("£49.99");
            shirtDesc.setText("Arsenal Home Kit Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            shirtImg.setImageResource(R.drawable.ars_shirt);
        }
        else if(team == "chs"){
            shirtName.setText("Chelsea Home Shirt");
            shirtPrice.setText("£55.00");
            shirtDesc.setText("Chelsea Home Kit Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            shirtImg.setImageResource(R.drawable.chs_shirt);
        }
        else if(team == "lvp"){
            shirtName.setText("Liverpool Home Shirt");
            shirtPrice.setText("£45.99");
            shirtDesc.setText("Liverpool Home Kit Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            shirtImg.setImageResource(R.drawable.lvp_shirt);
        }
        else if(team == "mci"){
            shirtName.setText("Manchester City Home Shirt");
            shirtPrice.setText("£49.99");
            shirtDesc.setText("Manchester City Home Kit Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            shirtImg.setImageResource(R.drawable.mci_shirt);
        }
        else if(team == "th"){
            shirtName.setText("Tottenham Home Shirt");
            shirtPrice.setText("£45.99");
            shirtDesc.setText("Tottenham Home Kit Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            shirtImg.setImageResource(R.drawable.tt_shirt);
        }
        else{
            shirtName.setText("Error");
            shirtPrice.setText("Error");
            shirtDesc.setText("Error");
        }


        return myView;
    }
}
