package com.example.owain.banking;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //finding the different elemets and attaching them to a variable
        final EditText etUsername = (EditText) findViewById(R.id.etEmail);
        final EditText etPassword = (EditText) findViewById(R.id.etSBal);
        final Button bLogin = (Button) findViewById(R.id.bSignIn);
        final TextView registerLink = (TextView) findViewById(R.id.tvRegisterLink);


        //setting up an onclick listener for the register textview
        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating an intent that points to the register activity
                Intent registerIntent = new Intent(MainActivity.this, RegisterActivity.class);
                MainActivity.this.startActivity(registerIntent);
            }
        });

        //setting up an on click listener
        bLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //getting inputted data
                final String username = etUsername.getText().toString();
                final String pin = etPassword.getText().toString();


                //response listener for the to get the request response
                Response.Listener<String> responseListener = new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        //Turn response into json and check it is successful
                        try {
                            JSONObject json = new JSONObject(response);
                            boolean success = json.getBoolean("success");

                            if(success){
                                //Assign JSON fields to variables
                                int userId = json.getInt("user_id");
                                String firstName = json.getString("first_name");
                                String lastName = json.getString("last_name");
                                String email = json.getString("email");
                                String password = json.getString("password");
                                String address = json.getString("address");
                                int phoneNumber = json.getInt("phone_number");
                                double balance = json.getDouble("balance");
                                int noTrans = json.getInt("no_transactions");
                                int points = json.getInt("points");
                                String permissions = json.getString("permissions");

                                //Creates new intent NavDrawer with variables
                                Intent intent = new Intent(MainActivity.this, NavDrawer.class);
                                intent.putExtra("username", email);
                                intent.putExtra("balance", balance);
                                intent.putExtra("first_name", firstName);
                                intent.putExtra("last_name", lastName);
                                intent.putExtra("pin", password);
                                intent.putExtra("userId", userId);
                                intent.putExtra("address", address);
                                intent.putExtra("phoneNumber", phoneNumber);
                                intent.putExtra("noTrans", noTrans);
                                intent.putExtra("points", points);
                                intent.putExtra("permissions", permissions);

                                MainActivity.this.startActivity(intent);
                            }
                            else{
                                //If Unsuccessfull show alert box saying login unsuccessful
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setMessage("Login Was Not Successful")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }
                        }
                        catch(JSONException e){
                            //Catch JSON error
                            e.printStackTrace();
                        }
                    }
                };

                //Login Request class is instantiated and request is added to queue
                LoginRequest loginRequest = new LoginRequest(username, pin, responseListener);
                RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
                queue.add(loginRequest);
            }
        });
    }
}
