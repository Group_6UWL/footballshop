package com.example.owain.banking;

/**
 * Created by owain on 14/02/2017.
 */

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class BagsFrag extends Fragment{
    public void openBag(String team){
        Bundle bundle = new Bundle();
        bundle.putString("team", team);
        Fragment bagFrag = new BagItemFrag();
        bagFrag.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, bagFrag).commit();
    }

    //Instantiates View
    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Sets View
        myView = inflater.inflate(R.layout.bags, container, false);

        final ImageButton arsenalBag = (ImageButton) myView.findViewById(R.id.arsBag);
        final ImageButton chelseaBag = (ImageButton) myView.findViewById(R.id.chsBag);
        final ImageButton liverpoolBag = (ImageButton) myView.findViewById(R.id.lvpBag);
        final ImageButton mcityBag = (ImageButton) myView.findViewById(R.id.mciBag);
        final ImageButton totBag = (ImageButton) myView.findViewById(R.id.ttBag);

        arsenalBag.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openBag("ars");
            }
        });

        chelseaBag.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openBag("chs");
            }
        });

        liverpoolBag.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openBag("lvp");
            }
        });

        mcityBag.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openBag("mci");
            }
        });

        totBag.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openBag("th");
            }
        });



        return myView;
    }
}