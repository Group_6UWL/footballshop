package com.example.owain.banking;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by owain on 12/12/2016.
 */

public class UserFrag extends Fragment {

    View myView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Sets View
        myView = inflater.inflate(R.layout.user, container, false);
        //Finds Elements
        final EditText fName = (EditText) myView.findViewById(R.id.etFName);
        final EditText lName = (EditText) myView.findViewById(R.id.etLName);
        final EditText username = (EditText) myView.findViewById(R.id.etUserName);
        final EditText pin = (EditText) myView.findViewById(R.id.etPin);
        Button update = (Button) myView.findViewById((R.id.bUpdate));

        //Sets Global Vraibales
        fName.setText(NavDrawer.firstName);
        lName.setText(NavDrawer.lastName);
        pin.setText(NavDrawer.pin+"");
        username.setText(NavDrawer.username);

        //sets onclick listener for pdate button
        update.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //gets inputted data
                final String first = fName.getText().toString();
                final String last = lName.getText().toString();
                final String user = username.getText().toString();
                final int pinNo = Integer.parseInt(pin.getText().toString());

                //sets response listener
                Response.Listener<String> responseListener = new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try {
                            //Turn response into json and check it is successful
                            JSONObject json = new JSONObject(response);
                            boolean success = json.getBoolean("success");

                            if(success){
                                //sets message and changes global variable of balance
                                NavDrawer.username = json.getString("new_user");
                                NavDrawer.pin = json.getInt("pin");
                                NavDrawer.firstName = json.getString("first_name");
                                NavDrawer.lastName = json.getString("last_name");
                                HomeFrag.message = "User Updated!";

                                //Sends back to home fragment
                                FragmentManager fragmentManager = getFragmentManager();
                                fragmentManager.beginTransaction().replace(R.id.content_frame, new HomeFrag()).commit();

                            }
                            else{
                                //if unsuccessful
                                HomeFrag.message = "Error - Could Not Update User";
                                FragmentManager fragmentManager = getFragmentManager();
                                fragmentManager.beginTransaction().replace(R.id.content_frame, new HomeFrag()).commit();
                            }
                        }
                        catch(JSONException e){
                            //catch json error
                            e.printStackTrace();
                        }
                    }
                };
                //Pay Request class is instantiated and request is added to queue
                UserRequest userRequest = new UserRequest(first,last,user, pinNo, NavDrawer.username, NavDrawer.balance, responseListener);
                RequestQueue queue = Volley.newRequestQueue(getActivity());
                queue.add(userRequest);
            }
        });
        return myView;
    }
}
