package com.example.owain.banking;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by owain on 14/03/2017.
 */

public class HatItemFrag extends Fragment{
    View myView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.hatitem, container, false);

        TextView hatName = (TextView) myView.findViewById(R.id.txtName);
        TextView hatPrice = (TextView) myView.findViewById(R.id.txtPrice);
        TextView hatDesc = (TextView) myView.findViewById(R.id.txtDescription);
        ImageView image = (ImageView) myView.findViewById(R.id.imgBag);

        String team = getArguments().getString("team");

        if(team == "ars"){
            hatName.setText("Arsenal Hat");
            hatPrice.setText("£14.99");
            hatDesc.setText("Arsenal Hat Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.ars_hat);
        }
        else if(team == "chs"){
            hatName.setText("Chelsea Hat");
            hatPrice.setText("£15.99");
            hatDesc.setText("Chelsea Hat Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.chs_hat);
        }
        else if(team == "lvp"){
            hatName.setText("Liverpool Hat");
            hatPrice.setText("£15.99");
            hatDesc.setText("Liverpool Hat Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.lvp_hat);
        }
        else if(team == "mci"){
            hatName.setText("Manchester City Hat");
            hatPrice.setText("£13.99");
            hatDesc.setText("Manchester City Hat Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.mci_hat);
        }
        else if(team == "th"){
            hatName.setText("Tottenham Hat");
            hatPrice.setText("£16.00");
            hatDesc.setText("Tottenham Hat Description Goes here - ljsdfklajf dfkhdsklfhds asklfhasklfjfklff sdfkhdkf sklsf dklhf skfsklfiouekfjn ksfheifh");
            image.setImageResource(R.drawable.tt_hat);
        }
        else{
            hatName.setText("Error");
            hatPrice.setText("Error");
            hatDesc.setText("Error");
        }

        return myView;
    }
}
